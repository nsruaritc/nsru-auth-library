<?php
namespace Nsru\Auth;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;

class NsruAuth
{
    public function __construct() {

    }

    public static function routes() {
        require(__DIR__.'/routes/web.php');
    }

    public static function authorize() {
        return \route("nsru-auth.authorize");
    }

    public static function handshake() {
        try {
            $accessToken = session()->get('nsru_auth.access_token');
            $getUrl = \config('nsru-auth.service_location')."/api/handshake";
            $response = Http::withoutVerifying()->acceptJson()->withToken($accessToken)->get($getUrl)->json();
            if($response['code'] ?? false == 'OK') {
                return true;
            } else {
                return false;
            }
        } catch(Exception $e) {
            return false;
        }
    }

    public static function terminate() {
        try {
            $accessToken = session()->get('nsru_auth.access_token');
            $getUrl = \config('nsru-auth.service_location')."/api/terminate";
            $response = Http::withoutVerifying()->acceptJson()->withToken($accessToken)->get($getUrl)->json();
            Auth::logout();
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }

}

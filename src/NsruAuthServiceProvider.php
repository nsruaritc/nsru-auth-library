<?php
namespace Nsru\Auth;

use Illuminate\Foundation\Console\AboutCommand;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Nsru\Auth\App\Console\Commands\Install as InstallCommand;


class NsruAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $router = $this->app['router'];

        // $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        // AboutCommand::add('Napadon Admin', fn () => [
        //     'Route name prefix' => config('napadon-admin.route_name_prefix'),
        //     'Theme'             => config('napadon-admin.theme'),
        // ]);

        // // ลงทะเบียน Middleware
        // $router->aliasMiddleware('auth.napadon-admin', \Napadon\Admin\App\Http\Middleware\Authenticate::class);

        // // ลงทะเบียน Component
        // Blade::componentNamespace('Napadon\Admin\App\View\Components', 'napadon-admin');

        // // ลงทะเบียน View
        // $this->loadViewsFrom(__DIR__.'/resources/views', 'napadon-admin');

        // // ลงทะเบียน Console Command
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class
            ]);
        }

        // ไฟล์ที่ต้อง Copy ไปยัง Project ที่ใช้งาน (ไม่ควรเขียนทับ)
        $this->publishes([
            __DIR__.'/config/nsru-auth.php' => config_path('nsru-auth.php')
        ], 'nsru-auth');

        // ไฟล์ที่ต้อง Copy ไปยัง Project ที่ใช้งาน (เขียนทับได้)
        $this->publishes([

        ], 'nsru-auth-overwrite');
    }
}

<?php
namespace Nsru\Auth\App\Helper;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class User
{
    public $userId;
    public $nsruAccountUsername;
    public $displayName;
    public $defaultBackgroundImageUrl;
    public $users;
    public $defaultAvatar;
    public $citizenId;

    public function __construct()
    {
        try {
            $nsruAuthData = \session()->get('nsru_auth');
            $accessToken = $nsruAuthData['access_token'];
            $url = Helper::apiEndpoint('user');
            $response = Http::withoutVerifying()->acceptJson()->withToken($accessToken)->get($url);
            $data = $response->json()['data'];
            $this->userId                       = $data['user_id']                      ?? '';
            $this->nsruAccountUsername          = $data['nsru_account_username']        ?? '';
            $this->displayName                  = $data['display_name']                 ?? '';
            $this->defaultBackgroundImageUrl    = $data['default_background_image_url'] ?? '';
            $this->users                        = $data['users']                        ?? '';
            $this->defaultAvatar                = $data['default_avatar']               ?? '';
            $this->citizenId                    = $data['citizen_id']                   ?? '';   
        } catch(Exception $e) {
            return false;
        }
    }

    public function isStudent() {
        return (\is_numeric($this->nsruAccountUsername));
    }

    public function isStaff() {
        return (!\is_numeric($this->nsruAccountUsername));
    }
}
<?php
namespace Nsru\Auth\App\Helper;

use Exception;
use Illuminate\Support\Facades\Http;
use Nsru\Auth\NsruAuth;

class Helper
{
    public static $apiEndpoint = "https://api.nsru.ac.th/auth/api/";
    
    public static function apiEndpoint($uri) {
        return Helper::$apiEndpoint.$uri;
    }

    public static function getLoginUrl() {
        return \route('nsru-auth.login');
    }

    /**
     * Undocumented function
     *
     * @deprecated 1.10.0
     * @return void
     */
    public static function handshake() {
        return NsruAuth::handshake();
    }

    /**
     * Undocumented function
     *
     * @deprecated 1.10.0
     * @return void
     */
    public static function terminate() {
        return NsruAuth::terminate();
    }
}
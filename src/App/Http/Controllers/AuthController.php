<?php
namespace Nsru\Auth\App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function authorizeStep(Request $request)
    {
        $request->session()->put('state', $state = Str::random(40));
        $query = http_build_query([
            'client_id'     => \config('nsru-auth.client_id'),
            'redirect_uri'  => route('nsru-auth.token'),
            'response_type' => 'code',
            'scope'         => \config('nsru-auth.scope'),
            'state'         => $state,
        ]);
        $redirectUrl = \config('nsru-auth.service_location').'/oauth/authorize?'.$query;
        return redirect($redirectUrl);
    }

    public function tokenStep(Request $request) {
        $state = $request->session()->pull('state');
        try {
            if($request->error != '') throw new Exception($request->message);
            throw_unless(strlen($state) > 0 && $state === $request->state, InvalidArgumentException::class);
            $response = Http::withoutVerifying()->asForm()->post(
                \config('nsru-auth.service_location').'/oauth/token',
                [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => \config('nsru-auth.client_id'),
                    'client_secret' => \config('nsru-auth.client_secret'),
                    'redirect_uri'  => route('nsru-auth.token'),
                    'code'          => $request->code,
                ]
            );
            if($response->ok()) {
                $request->session()->put('nsru_auth', $response->json());
                return redirect()->route('nsru-auth.login');
            } else {
                throw new Exception('Error: '.$response->status());
            }
        } catch(Exception $e) {
            if($e->getMessage() == "The resource owner or authorization server denied the request.") {
                Log::error($e->getMessage(), [
                    'state' => $state,
                    'request_state' => $request->state,
                    'request_input' => $request->all(),
                ]);
                return redirect()->route(\config('nsru-auth.route_to_login_ui'))->with('error', $e->getMessage());
            }
            if($e->getMessage() == "Nsru\Auth\App\Http\Controllers\InvalidArgumentException") {
                return \redirect()->route('nsru-auth.authorize');
            }

            Log::error($e->getMessage(), [
                'state' => $state,
                'request_state' => $request->state,
                'request_input' => $request->all(),
            ]);
            return redirect()->route(\config('nsru-auth.route_to_login_ui'))->with('error', $e->getMessage());
        }
    }

    public function loginStep(Request $request) {
        try {
            $accessToken = $request->session()->get('nsru_auth.access_token');
            $response = Http::withoutVerifying()->withHeaders([
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ])->get(\config('nsru-auth.service_location').'/api/user');

            $responseArray = \json_decode($response->body(), true);

            if(isset($responseArray['data'])) {
                $email = $responseArray['data']['nsru_account_username'].'@nsru.local';
                if($user = User::where('email', $email)->first()) {
                    $user->name     = $responseArray['data']['display_name'];
                    $user->save();
                } else {
                    $user = new User();
                    $user->email    = $email;
                    $user->name     = $responseArray['data']['display_name'];
                    $user->password = bcrypt(Str::random(40));
                    $user->save();
                }
                Auth()->login($user);
                return redirect()->route(\config('nsru-auth.route_after_login_success'))->with('success', 'Login success');
            } else {
                throw new Exception('Invalid response from NSRU Auth Service.'.json_encode($responseArray));
            }
        } catch(Exception $e) {
            return \redirect()->route(\config('nsru-auth.route_to_login_ui'))->with('error', $e->getMessage());
        }
    }
}

<?php

namespace Nsru\Auth\App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nsru-auth:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install NSRU Auth';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Publish NapadonAdmin assets, database, and config files.
        $this->line('Publishing NsruAuth config files');
        $this->callSilent('vendor:publish', ['--tag' => 'nsru-auth',            '--force' => false  ]);
        $this->callSilent('vendor:publish', ['--tag' => 'nsru-auth-overwrite',  '--force' => true   ]);

        // Add NapadonAdmin::routes() to routes/web.php
        $web = file_get_contents(base_path('routes/web.php'));
        $routeToAdd = '\Nsru\Auth\NsruAuth::routes();';
        if (strpos($web, $routeToAdd) === false) {
            $web .= "\n" . $routeToAdd;
            file_put_contents(base_path('routes/web.php'), $web);
            $this->info('Add NapadonAdmin::routes() to routes/web.php');
        } else {
            $this->line('NapadonAdmin::routes() is already added');
        }

        // Migrate database.
        // $this->line('Migrating the database tables into your application');
        // $this->callSilent('migrate');

        // Create admin user.
        // if( ! User::where('email', 'admin@local')->first() ) {
        //     $this->info('Creating admin@local user');
        //     $user = new User();
        //     $user->name = 'Administrator';
        //     $user->email = 'admin@local';
        //     $user->password = bcrypt('admin');
        //     $user->save();
        // } else {
        //     $this->line('admin@local user already exists');
        // }

        // .gitignore
        // $gitignore = file_get_contents(base_path('.gitignore'));
        // $gitignoreToAdd = '/public/vendor';
        // if (strpos($gitignore, $gitignoreToAdd) === false) {
        //     $gitignore .= "\n" . $gitignoreToAdd;
        //     file_put_contents(base_path('.gitignore'), $gitignore);
        //     $this->info('Add public/vendor to .gitignore');
        // } else {
        //     $this->line('public/vendor is already added to .gitignore');
        // }

        // Publishing spatie/permission
        // $this->line('Publishing spatie/permission assets, database, and config files');
        // $this->callSilent('vendor:publish', ['--provider' => 'Spatie\Permission\PermissionServiceProvider', '--force' => true]);
        // $this->callSilent('config:clear');
        // $this->callSilent('migrate');

        // add use HasRoles to app/Models/User.php
        // $user = file_get_contents(base_path('app/Models/User.php'));
        // $target = 'use HasApiTokens, HasFactory, Notifiable;';
        // $targetToAdd = 'use \Spatie\Permission\Traits\HasRoles;';
        // if (strpos($user, $targetToAdd) === false) {
        //     $user = str_replace($target, $target . "\n\t" . $targetToAdd, $user);
        //     file_put_contents(base_path('app/Models/User.php'), $user);
        //     $this->info('Add use HasRoles to app/Models/User.php');
        // } else {
        //     $this->line('use HasRoles is already added to app/Models/User.php');
        // }

        // Add role
        // $this->line('Add role');
        // $this->callSilent('permission:create-role', ['name' => 'Administrator']);
        // $this->callSilent('permission:create-role', ['name' => 'User']);

        // Add role to Admin User
        // $this->line('Add role to Admin User');
        // $admin = User::where('email', 'admin@local')->first();
        // $admin->assignRole('User');
        // $admin->assignRole('Administrator');

        $this->line('Bye.');
        return Command::SUCCESS;
    }
}

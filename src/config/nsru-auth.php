<?php

return [

    /** ===================================================================================================
     * ข้อมูลของ NSRU OAuth2 Server
     * ====================================================================================================
     */

    'service_location'          => 'https://api.nsru.ac.th/auth',

    // Client Id ขอได้จาก https://api.nsru.ac.th/auth
    'client_id'                 => env('NSRU_AUTH_CLIENT_ID',       ''),

    // Client Secret ขอได้จาก https://api.nsru.ac.th/auth
    'client_secret'             => env('NSRU_AUTH_CLIENT_SECRET',   ''),

    // ขอบเขตของการเข้าถึงข้อมูล
    'scope'                     => env('NSRU_AUTH_SCOPE',           'self-basic'),

    /** ===================================================================================================
     * กลุ่มของ Route
     * ====================================================================================================
     */

    // ชื่อ Route ของหน้าล๊อกอินของระบบ
    'route_to_login_ui'         => 'napadon-admin.login',

    // หลังจากที่ login สำเร็จแล้ว จะ redirect ไปยัง route นี้
    'route_after_login_success' => 'napadon-admin.me.dashboard',

];

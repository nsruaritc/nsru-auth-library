<?php

use Illuminate\Support\Facades\Route;
use Nsru\Auth\App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/vendor/nsru/auth/authorize', [ AuthController::class, 'authorizeStep'   ])->name("nsru-auth.authorize");
Route::get('/vendor/nsru/auth/token',     [ AuthController::class, 'tokenStep'       ])->name("nsru-auth.token");
Route::get('/vendor/nsru/auth/login',     [ AuthController::class, 'loginStep'       ])->name("nsru-auth.login");
Route::get('/vendor/nsru/auth/logout',    [ AuthController::class, 'logoutStep'       ])->name("nsru-auth.logout");